ARG DOCKER_ALPINE_VERSION

FROM maurosoft1973/alpine:$DOCKER_ALPINE_VERSION

ARG BUILD_DATE
ARG ALPINE_ARCHITECTURE
ARG ALPINE_RELEASE
ARG ALPINE_VERSION
ARG ALPINE_VERSION_DATE
ARG NET_RELEASE
ARG NET_VERSION
ARG NET_VERSION_DATE

ENV ENV_SH="/root/.shrc"
ENV ENV_BASH="/root/.bashrc"

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="$ALPINE_ARCHITECTURE" \
    net-version="$NET_VERSION" \
    alpine-version="$ALPINE_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="alpine-net-sonarqube-scanner" \
    org.opencontainers.image.description=".NET $NET_VERSION Sonarqube Scanner Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$NET_VERSION" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/alpine-net-sonarqube-scanner" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/alpine-net-sonarqube-scanner" \
    org.opencontainers.image.created=$BUILD_DATE

RUN \
    echo "" > /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/main" >> /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/community" >> /etc/apk/repositories && \
    apk update && \
    apk add --update --no-cache \
    openjdk17-jre \
    krb5-pkinit \
    krb5-dev \
    krb5 \
    libxml2 \
    openssh-client \
    ca-certificates \
    sshpass \
    curl \
    rsync \
    openssh \
    wget \
    bash \
    icu-libs \
    krb5-libs \
    libgcc \
    libintl \
    libssl1.1 \
    libstdc++ \
    zlib \
    zip && \
    wget https://dot.net/v1/dotnet-install.sh -O ./dotnet-install.sh && \
    chmod +x ./dotnet-install.sh && ./dotnet-install.sh -c $NET_RELEASE

RUN /root/.dotnet/dotnet tool install --tool-path /scripts dotnet-sonarscanner || echo "KO" > /dotnet-sonarscanner.ko
RUN /root/.dotnet/dotnet tool install --tool-path /scripts dotnet-coverage || echo "KO" > /dotnet-coverage.ko

COPY files/ /scripts
RUN chmod +x /scripts/run-alpine-sonarqube-scanner.sh

ENTRYPOINT ["/scripts/run-alpine-sonarqube-scanner.sh"]
