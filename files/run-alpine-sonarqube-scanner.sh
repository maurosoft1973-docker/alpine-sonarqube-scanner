#!/bin/sh
source /scripts/init-alpine.sh

export SHELL_TERMINAL=${SHELL_TERMINAL:-"/bin/sh"}

export DOTNET_ROOT=/root/.dotnet
export PATH=$PATH:$DOTNET_ROOT:/scripts

mkdir /public

$SHELL_TERMINAL