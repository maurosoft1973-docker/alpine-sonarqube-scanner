# .NET Sonarqube Scanner Docker image running on Alpine Linux

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/alpine-net-sonarqube-scanner.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-net-sonarqube-scanner/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/alpine-net-sonarqube-scanner.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-net-sonarqube-scanner/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/alpine-net-sonarqube-scanner.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-net-sonarqube-scanner/)

[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v3.17.3-green.svg?style=for-the-badge)](https://alpinelinux.org/)

This Docker image [(maurosoft1973/alpine-net-sonarqube-scanner)](https://gitlab.com/maurosoft1973-docker/alpine-net-sonarqube-scanner/) is based on the minimal [Alpine Linux](https://alpinelinux.org/) with [.NET Version v8.0.4](https://dotnet.microsoft.com/en-us/).

##### Alpine Version 3.17.3 (Released Mar 29 2023)
##### .NET Version 8.0.4 (Released 2023-04-09 00:00:00)

----

## Description

This image include the latest sonarqube scanner for .Net

## Architectures

* ```:aarch64``` - 64 bit ARM
* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest```         latest branch based (Automatic Architecture Selection)
* ```:aarch64```        latest 64 bit ARM
* ```:x86_64```         latest 64 bit Intel/AMD
* ```:test```           test branch based (Automatic Architecture Selection)
* ```:test-aarch64```   test 64 bit ARM
* ```:test-x86_64```    test 64 bit Intel/AMD
* ```:8.0.4``` 8.0.4 branch based (Automatic Architecture Selection)
* ```:8.0.4-aarch64```   8.0.4 64 bit ARM
* ```:8.0.4-x86_64```    8.0.4 64 bit Intel/AMD
* ```:8.0.4-3.17.3``` 8.0.4-3.17.3 branch based (Automatic Architecture Selection)
* ```:8.0.4-3.17.3-aarch64```   8.0.4-3.17.3 64 bit ARM
* ```:8.0.4-3.17.3-x86_64```    8.0.4-3.17.3 64 bit Intel/AMD

## Howto use this image?

This image creates containers that allow you to scan .net project inside container with Sonarqube.

## Environment Variables:

### Main Alpine .Net parameters:
* `LC_ALL`: default locale (default en_GB.UTF-8)
* `TIMEZONE`: default timezone (default Europe/Brussels)
* `SHELL_TERMINAL`: shell container (default /bin/sh)

## Example of use

### 1. Run container inside GitLab Pipeline for Test with Report and Coverage

This job use the image maurosoft1973/alpine-net-sonarqube-scanner for scan project and send report to Sonarqube

```pipeline
quality:
    stage: test
    image: maurosoft1973/alpine-net-sonarqube-scanner
    services:
        - name: mcr.microsoft.com/mssql/server:2017-latest
          alias: db
    variables:
        SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
        GIT_DEPTH: "0"
    allow_failure: true
    only:
        - master
        - merge_request
    needs: ["unit-test","integration-test"]
    cache:
        key: "${CI_JOB_NAME}"
        paths:
            - .sonar/cache
    script:
      - dotnet sonarscanner begin /k:"$SONAR_PROJECT" /d:sonar.login="$SONAR_TOKEN" /d:sonar.host.url="$SONAR_HOST_URL" /d:sonar.cs.vscoveragexml.reportsPaths=coverage.xml
      - dotnet build --no-incremental
      - dotnet-coverage collect 'dotnet test' -f xml  -o 'coverage.xml'
      - dotnet sonarscanner end /d:sonar.login="$SONAR_TOKEN"

```

***
###### Last Update 10.03.2025 22:02:42